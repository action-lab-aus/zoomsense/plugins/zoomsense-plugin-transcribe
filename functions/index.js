const functions = require('firebase-functions');
const environment = functions.config()[process.env.GCLOUD_PROJECT];
const admin = require('firebase-admin');
admin.initializeApp();
const db = admin.database();
const path = require('path');
const AWS = require('aws-sdk');
AWS.config.loadFromPath(path.join(__dirname, '/awsConfig.json'));
const S3 = new AWS.S3();
const BUCKET_NAME = environment.aws.bucket_name;
const { default: srtParser2 } = require('srt-parser-2');
const parser = new srtParser2();
const request = require('request');

/**
 * When recording status has been added as `uploaded`, call AWS Transcribe to start the
 * transcription if the plugin is enabled
 *
 * @param {String} meetingId Session ID
 */
exports.onRecordingUploaded = functions.database
  .ref('/data/recording/{meetingId}/status')
  .onCreate(async (snapshot, context) => {
    try {
      const meetingId = context.params.meetingId;
      const status = snapshot.val();
      if (status === 'uploaded') {
        const recording = (await db.ref(`/data/recording/${meetingId}`).once('value')).val();

        // Fetch the current plugins object
        const plugins = (await db.ref(`/config/${meetingId}/current/currentState/plugins`).once('value')).val();
        if (plugins['transcribe'] && plugins['transcribe'].enabled) {
          // If the transcribe plugin is enabled for the current session, start the transcription
          const s3ObjKey = recording.s3UrlVideo;
          const s3Url = `s3://${BUCKET_NAME}/${s3ObjKey}`;
          const fileName = `${s3ObjKey.split('/')[1]}.mp4`;

          functions.logger.info('s3Url: ', s3Url);
          functions.logger.info('fileName: ', fileName);
          functions.logger.info('s3ObjKey: ', s3ObjKey);

          await createTranscriptionJob(s3Url, fileName, s3ObjKey);
        }
      }
    } catch (error) {
      functions.logger.error('onRecordingUploaded Error: ', error);
    }
  });

/**
 * Create the AWS transcribe job for the session archive
 *
 * @param {String} url S3 URL of the video
 * @param {String} fileName Transcription job name
 * @param {String} filePath Video file path in S3
 */
async function createTranscriptionJob(url, fileName, filePath) {
  const transcribeService = new AWS.TranscribeService();
  const TranscriptionJobName = fileName;
  const filePathArr = filePath.split('/');

  const transcriptionJob = {
    IdentifyLanguage: true,
    Media: { MediaFileUri: url },
    MediaFormat: 'mp4',
    TranscriptionJobName,
    OutputBucketName: BUCKET_NAME,
    OutputKey: `${filePathArr[0]}/${filePathArr[1]}/`,
    Subtitles: {
      Formats: ['srt'],
    },
  };

  try {
    await transcribeService
      .getTranscriptionJob({
        TranscriptionJobName,
      })
      .promise();

    // Remove the existing transcription job if it already exists
    await transcribeService.deleteTranscriptionJob({ TranscriptionJobName }).promise();
    return transcribeService.startTranscriptionJob(transcriptionJob).promise();
  } catch (error) {
    // If error occurred, the job does not exist and no action needed
    return transcribeService.startTranscriptionJob(transcriptionJob).promise();
  }
}

/**
 * AWS SNS notification endpoint triggered when the AWS Transcribe job is either completed
 * or failed. Record the transcription status under the `/data/transcription` node
 */
exports.transcriptionOnCompletedSns = functions.https.onRequest(async (req, res) => {
  try {
    const payload = JSON.parse(req.body);

    if (req.header('x-amz-sns-message-type') === 'SubscriptionConfirmation') {
      console.log(payload);
      const url = payload.SubscribeURL;
      await request(url, handleSubscriptionResponse);
      return res.send('OK');
    } else {
      // Parse the request payload and extract the notification topic
      const topic = payload.TopicArn.split(':')[5];
      functions.logger.info('topic: ', topic);

      if (topic === 'TopsTranscribeCompleteAlert') {
        const message = JSON.parse(payload.Message);
        // functions.logger.info('message: ', message);

        let s3ObjKey = message.Records[0].s3.object.key;
        s3ObjKey = s3ObjKey.substring(0, s3ObjKey.length - 4) + 'srt';
        const archiveId = s3ObjKey.split('/')[1];

        // Fetch the coach uid & meeting id from the archive reference node
        const snapshot = await db.ref(`archiveRef/${archiveId}`).once('value');
        const { meetingId } = snapshot.val();

        // Get the transcription json file
        const TranscriptFileJSON = await getAWSTranscript(s3ObjKey);
        // functions.logger.info('TranscriptFileJSON: ', TranscriptFileJSON);

        // Update the transcription data node with the transcribed status
        await db.ref(`data/transcription/${meetingId}`).set({
          raw: TranscriptFileJSON,
          status: 'transcribed',
          s3UrlTranscription: s3ObjKey,
        });
      } else if (topic === 'TranscribeJobErrorAlert') {
        const message = JSON.parse(payload.Message);
        // functions.logger.info("message: ", message);

        const archiveId = message.detail.JobName.split('.')[0];
        functions.logger.info('archiveId: ', archiveId);

        // Fetch the coach uid & meeting id from the archive reference node
        const snapshot = await db.ref(`archiveRef/${archiveId}`).once('value');
        const { meetingId } = snapshot.val();

        // Update the curationAssets data node with the transcribed status
        await db.ref(`data/transcription/${meetingId}`).set({
          status: 'error',
          error: message.detail.LanguageIdentificationStatus,
        });
      }

      res.status(200).send('Transcription uploaded to S3');
    }
  } catch (error) {
    functions.logger.error(error);
    res.status(500).send(error);
  }
});

/**
 * Get the transcript created by AWS Transribe with the given S3 object key
 *
 * @param {*} s3ObjectKey S3 object key
 */
async function getAWSTranscript(s3ObjectKey) {
  const params = {
    Bucket: BUCKET_NAME,
    Key: s3ObjectKey,
  };

  // Download the transcript file and parse the object
  const TranscriptFile = await S3.getObject(params).promise();
  return parser.fromSrt(TranscriptFile.Body.toString('utf-8'));
}

// // AWS SNS endpoint for subscription verficiation
// exports.transcriptionOnCompletedSns = functions.https.onRequest(async (req, res) => {
//   let payload = JSON.parse(req.body);
//   console.log('payload: ', payload);
//   try {
//     if (req.header('x-amz-sns-message-type') === 'SubscriptionConfirmation') {
//       const url = payload.SubscribeURL;
//       await request(url, handleSubscriptionResponse);
//     } else if (req.header('x-amz-sns-message-type') === 'Notification') {
//       console.log(payload);
//     } else {
//       throw new Error(`Invalid message type ${payload.Type}`);
//     }
//   } catch (err) {
//     console.error(err);
//     res.status(500).send('Oops');
//   }
//   res.send('Ok');
// });

const handleSubscriptionResponse = function (error, response) {
  if (!error && response.statusCode === 200) {
    console.log('Yes! We have accepted the confirmation from AWS.');
  } else {
    throw new Error(`Unable to subscribe to given URL`);
  }
};

# Bicara Plugin - Transcribe

Bicara Transcribe Plugin actively checks whether a session recording has been uploaded to the S3 bucket and triggers AWS Transcribe if enabled.

The plugin receives notification when the transcription job is either completed (when `.json` is pushed to a dedicated S3 bucket) or failed (when language identification failed for the source `.mp4` file), and it records the status under `/data/transcription/${meetingId}` for triggering curation ready/issue emails.

More information about the transcription workflow can be found under [Bicara Core Step 2.4](https://gitlab.com/action-lab-aus/bicara/zoomsense-tops/-/blob/main/docs/3_QuickStart_Vonage.md).

## Local Development

Once the `.runtimeconfig.json` file is prepared under the `/functions` folder (configuration details can be found under [Bicara Core Step 6](https://gitlab.com/action-lab-aus/bicara/zoomsense-tops/-/blob/main/docs/7_QuickStart_Functions.md)), Firebase Functions can be tested locally using the command:

```
npm run emulators
```

This ensures that the emulator data will be imported when Firebase Emulator starts and exported when the session ends.
